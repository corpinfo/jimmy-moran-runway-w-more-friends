
  data "aws_cloudformation_export" "PrivateSubnet1" {
  name = "jimm3037template-PrivSub1"
  }
  
  data "aws_cloudformation_export" "PrivateSubnet2" {
  name = "jimm3037template-PrivSub2"
  }

  data "aws_cloudformation_export" "PublicSubnet1" {
  name = "jimm3037template-PubSub1"
  }
  
  data "aws_cloudformation_export" "PublicSubnet2" {
  name = "jimm3037template-PrivSub2"
  }
  data "aws_cloudformation_export" "VPC" {
  name = "jimm3037template-vpcId"
  }
  data "aws_cloudformation_export" "AZ1" {
  name = "jimm3037template-AZ1"
  }
 data "aws_cloudformation_export" "AZ2" {
  name = "jimm3037template-AZ2"
  }



###################################
#Elastic Load Balancer
###################################

resource "aws_elb" "this" {
  name = "${terraform.workspace}-lb"
  security_groups = [aws_security_group.lb_sg.id, aws_security_group.ec2_sg.id]
  cross_zone_load_balancing = true
  subnets = [data.aws_cloudformation_export.PublicSubnet1.value, data.aws_cloudformation_export.PublicSubnet2.value]
  listener {
    instance_port = 80
    instance_protocol = "HTTP"
    lb_port = 80
    lb_protocol = "HTTP"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 5
    target = "HTTP:80/"
    interval = 10
  }
}





#resource "aws_lb" "this" {
#  name               = "${terraform.workspace}-lb"
#  load_balancer_type = "application"
#  subnets            = [data.aws_cloudformation_export.PublicSubnet1.value, data.aws_cloudformation_export.PublicSubnet2.value]
#  security_groups    = [aws_security_group.lb_sg.id, aws_security_group.ec2_sg.id] 
#  internal           = false
#}
#
#resource "aws_lb_listener" "front_end" {
#  load_balancer_arn = aws_lb.this.arn
#  port              = "80"
#  protocol          = "HTTP"
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.lb_target.arn
#  }
#}
#
resource "aws_lb_target_group" "lb_target" {
  name     = "imgmgr-${terraform.workspace}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_cloudformation_export.VPC.value
}


############################################################
###########################################################

resource "aws_security_group" "lb_sg" {
  name        = "lb_sg"
  description = "Allow http inbound traffic"
  vpc_id      = data.aws_cloudformation_export.VPC.value

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "ec2_sg" {
  name        = "ec2_sg"
  description = "Allow http inbound traffic"
  vpc_id      = data.aws_cloudformation_export.VPC.value

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.lb_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


###################################
#Launchconfiguration
###################################
resource "aws_launch_configuration" "this" {
  name_prefix          = "terraform-lc-example-"
  image_id             = var.ami
  instance_type        = var.ASGInstanceType
  iam_instance_profile = aws_iam_instance_profile.server_profile.id
  security_groups      = [aws_security_group.ec2_sg.id] 
  user_data            = data.template_file.user_data.rendered
  key_name             = var.key_name
  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    S3Bucket = data.aws_cloudformation_stack.mint-dev.outputs["MoneyBucket"]
    QUEUE_URL = data.aws_cloudformation_stack.mint-dev.outputs["WorkerQueueName"]
  }
}

###################################
#AutoscalingGroup
###################################
resource "aws_autoscaling_group" "this" {
  name_prefix               = "${terraform.workspace}-asg-"
  max_size                  = var.ASGMaxCount
  min_size                  = var.ASGMinCount
  target_group_arns         = [aws_lb_target_group.lb_target.arn]
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = var.ASGDesired
  force_delete              = true
  launch_configuration      = aws_launch_configuration.this.id
  vpc_zone_identifier       = [data.aws_cloudformation_export.PrivateSubnet1.value, data.aws_cloudformation_export.PrivateSubnet2.value]

}
