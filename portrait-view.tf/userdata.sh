#!/bin/sh
GIT_REPO_NAME=crypto-portraits
SERVICE_NAME=crypto-portraits
DEPLOY_USER=ec2-user


yum install -y git jq
amazon-linux-extras install -y nginx1
pip3 install pipenv
PATH=$PATH:/usr/local/bin
cd /home/$DEPLOY_USER

git clone https://github.com/tonyfruzza/$GIT_REPO_NAME.git
cd $GIT_REPO_NAME
chown -R $DEPLOY_USER:$DEPLOY_USER .
cat > run.sh << EOF
#!/bin/sh
pipenv install
pipenv run pip3 install -r requirements.txt
pipenv run flask run
EOF
chmod 755 run.sh

echo "BUCKET_NAME=${S3Bucket}" >> .flaskenv
echo "SQS_NAME=${QUEUE_URL}" >> .flaskenv

cat > /etc/systemd/system/$SERVICE_NAME.service << EOF
[Unit]
Description=$SERVICE_NAME app
After=network.target
[Service]
User=$DEPLOY_USER
WorkingDirectory=/home/$DEPLOY_USER/$GIT_REPO_NAME
ExecStart=/home/$DEPLOY_USER/$GIT_REPO_NAME/run.sh
Restart=always
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl start $SERVICE_NAME
cat > /etc/nginx/conf.d/myapp.conf << EOF
server {
   listen 80;
   server_name localhost;
   location / {
        proxy_set_header Host \$http_host;
        proxy_pass http://127.0.0.1:5000;
    }
}
EOF
systemctl restart nginx.service
