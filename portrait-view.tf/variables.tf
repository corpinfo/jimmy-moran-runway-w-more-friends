# Variable definitions
variable "ec2_size" {
  default = "t3.micro"
}
# Variable definitions
variable "region" {
  type    = string
  default = "us-east-1"
}
variable "bucket" {
  type    = string
  default = "imgmgjimmy3037"
}
variable "ami" {
  type    = string
  default = "ami-02e136e904f3da870"
}
variable "ec2_ami_id" {
  type    = string
  default = "ami-02e136e904f3da870"
}
variable "s3bucket" {
  type    = string
  default = "imgmgjimmy3038"
}

variable "ASGMinCount" {
  type        = number
  default     = 1
  description = "ASG Minimum Server Count"
}

variable "ASGMaxCount" {
  type        = number
  default     = 2
  description = "ASG Minimum Server Count"
}
variable "ASGDesired" {
  type        = number
  default     = 1
  description = "ASG Desired Server Count"
}
variable "ASGInstanceType" {
  default     = "t3.micro"
  description = "ASG Launch Config Type"
}
 variable "key_name"  {
  default     = "east1key"
  description = "ec2 key"
}
