output "outputs" {
    description = "outputs"
    value = data.aws_cloudformation_stack.mint-dev.outputs
}

output "WorkerQueue" {
    description = "WorkerQueue"
    value = data.aws_cloudformation_stack.mint-dev.outputs["WorkerQueueName"]
}

output "MemeBucketArn" {
    description = "MoneyBucketArn"
    value = data.aws_cloudformation_stack.mint-dev.outputs["MoneyBucketArn"]
}

